import curses

class SnakeBody:
    
    def __init__(self, screen):
        #set screen to draw to
        self.screen = screen
        #initialize body
        self.snakebody = [[4,5], [4,4], [4,3], [4,2], [4,1]]
        self.snakesize = len(self.snakebody)
        for b in range(self.snakesize):
            self.drawBody(b)
        
        #initialize body Matrix
        row, col = screen.getmaxyx()
        self.bodyMatrix = [[0 for i in range(row)] for j in range(col)]
        for pos in self.snakebody:
            self.setBodyMatrix(pos[0], pos[1], 1)

    def drawBody(self, bodyID):
        pos = self.snakebody[bodyID]
        self.screen.addstr(pos[1],pos[0], "  ", curses.A_REVERSE)
        head = self.snakebody[0]
        self.screen.addstr(0, 50, "Snake Pos: {0}, {1} ".format(head[0], head[1]))
    
    def clearBody(self, bodyID):
        pos = self.snakebody[bodyID]
        self.screen.addstr(pos[1],pos[0], "  ")
        self.setBodyMatrix(pos[0], pos[1], 0)

    def moveSnake(self, dx, dy):
        self.clearBody(len(self.snakebody)-1)#undraw the tail

        #draw new head
        #shift snake body cells
        self.snakebody.pop(self.snakesize - 1)#pop tail
        head = self.snakebody[0]#save head

        self.snakebody.insert(0, [head[0]+dx*2, head[1]+dy])#append new head
        #set body matrix
        head = self.snakebody[0]
        self.setBodyMatrix(head[0], head[1], 1)

        self.drawBody(0)

    def addLength(self, dx, dy):
        head = self.snakebody[0]#save head

        self.snakebody.insert(0, [head[0]+dx*2, head[1]+dy])#append new head
        self.snakesize += 1
        #set body matrix
        head = self.snakebody[0]
        self.setBodyMatrix(head[0], head[1], 1)

        self.drawBody(0)

    def snakeColided(self, dx, dy):
        head = self.snakebody[0]
        row, col = self.screen.getmaxyx()
        if (head[0] + 2*dx >= col) or (head[1] + dy >= row) or (head[0] + dx < 0) or (head[1] + dy < 0):#Check out of bounds
            print("Collided with wall")
            return True
        elif self.bodyMatrix[head[0] + 2*dx][head[1] + dy] == 1:#Check head and body collision
            print("Colided with body")
            return True
        else:
            return False

    def setBodyMatrix(self, x, y, val):
        self.bodyMatrix[x][y] = val

    def printBodyMatrix(self):
        print('\n'.join([''.join(['{:4}'.format(item) for item in row]) for row in self.bodyMatrix]))