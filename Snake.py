import curses
import time

import SnakeInput as SI
import SnakeBody
import Apple

screen = curses.initscr()
curses.noecho()
curses.cbreak()
curses.curs_set(0)

frames=0#frame count for fps limiting
framecount=0#frame count for movement
lastFPS=time.time()
lastLoop=time.time()
fps=0
flag=True
snakedir = [0,0] #snake position y x
adjustmentTime = 0

fpsString = "FPS: 0"
screen.addstr(0,0, fpsString)

snake = SnakeBody.SnakeBody(screen)
apple = Apple.Apple(screen, snake)

def handleInput():
    #Handle input
    input = SI.getInput()
        
    if(input == 'w' and snakedir[1] != 1):
        snakedir[1] = -1
        snakedir[0] = 0
    elif(input == 'a' and snakedir[0] != 1):
        snakedir[0] = -1
        snakedir[1] = 0
    elif(input == 's' and snakedir[1] != -1):
        snakedir[1] = 1
        snakedir[0] = 0
    elif(input == 'd' and snakedir[0] != -1):
        snakedir[0] = 1
        snakedir[1] = 0
    else:
        input = ''

#Main game loop
while flag:
    #exit if quitting
    if(SI.getInput() == 'q'):
        break

    #Update frame count
    frames += 1
    framecount += 1
    #Get fps and print it
    deltatime = (time.time() - lastLoop)
    lastFPS += deltatime
    lastLoop = time.time()

    #a second has passed
    if(lastFPS >= 1):
        fps = frames
        fpsString = "FPS: {0}".format(fps)
        screen.addstr(0,0, fpsString)
        lastFPS = 0
        frames = 0
        canMove = True

    #get and handle input
    handleInput()
    if((framecount % 4) == 0):
        framecount = 0
        if snakedir[0] != 0 or snakedir[1] != 0:
            if snake.snakeColided(snakedir[0], snakedir[1]):
                flag = False
            else:
                snake.moveSnake(snakedir[0], snakedir[1])

        #check if snake gets apple
        if apple.checkSnake():
            snake.addLength(snakedir[0], snakedir[1])
            apple.spawnNew()

    #display which key was pressed and snake position
    screen.addstr(0, 40, "Direction: {0}, {1} ".format(snakedir[1], snakedir[0]))

    screen.refresh()

    #wait to approach a 60 fps
    sleepTime = 1/90 - (time.time() - lastLoop) #optimal time
    if(sleepTime > 0):
        time.sleep(sleepTime)

screen.refresh()
curses.napms(1500)
curses.endwin()