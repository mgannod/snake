import curses
import SnakeBody
import random

class Apple:

    def __init__(self, screen, snake):
        self.pos = [0,0]
        self.screen = screen
        self.snake = snake
        self.row, self.col = screen.getmaxyx()
        random.seed()
        self.spawnNew()

    def drawApple(self):
        self.screen.addstr(0, 15, "Apple Pos: {0},{1} ".format(self.pos[0], self.pos[1]))
        self.screen.addstr(self.pos[1], self.pos[0], "[]")

    def spawnNew(self):
        newx = random.randrange(0, self.col-2, 2)
        newy = random.randrange(1, self.row)
        self.pos = [newx, newy]
        if self.snake.bodyMatrix [self.pos[0]][self.pos[1]] == 1:
            self.spawnNew()
        else:
            self.drawApple()
        
    def checkSnake(self):
        if self.snake.snakebody[0] == self.pos:
            return True
        else:
            return False