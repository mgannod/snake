import keyboard

#return if q w a s or d is pressed otherwise return ' '
def getInput():
    if(keyboard.is_pressed('q')):
        return 'q'
    elif(keyboard.is_pressed('w')):
        return 'w'
    elif(keyboard.is_pressed('a')):
        return 'a'
    elif(keyboard.is_pressed('s')):
        return 's'
    elif(keyboard.is_pressed('d')):
        return 'd'
    elif(keyboard.is_pressed('e')):
        return 'e'
    else:
        return ' '